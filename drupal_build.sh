#!/bin/bash
###############################################################################
# Name:		drupal_build.sh
# Purpose:	Build a drupal distribution
###############################################################################

# Source the configuration files
# First, figure out where this script is located regardless of where it is 
# called. Then, call the load configuration script relative to the path
# of this script.
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ] ; do SOURCE="$(readlink "$SOURCE")"; done
TOOLS_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

# Load the tools
. $TOOLS_DIR/lib/print.sh
. $TOOLS_DIR/lib/load_configuration.sh
. $TOOLS_DIR/lib/checks.sh
. $TOOLS_DIR/lib/git.sh
. $TOOLS_DIR/lib/db.sh
. $TOOLS_DIR/lib/build_util.sh
. $TOOLS_DIR/lib/build.sh
. $TOOLS_DIR/lib/ui.sh

load_configuration

if in_a_build; then
	show_build_menu
else
	get_current_branch
	get_current_release
	set_build_paths
	set_build_name
	
	show_project_menu
fi
