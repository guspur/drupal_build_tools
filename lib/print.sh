#!/bin/bash
###############################################################################
# Name:		print.sh
# Purpose:  Utiltiy to print colored messages
###############################################################################

# COLORS
RED=$(tput setaf 1)
YELLOW=$(tput setaf 3)
WHITE=$(tput setaf 7)
GREEN=$(tput setaf 2)

function print_error() {
	if [[ "$1" ]]; then
		echo -e "${RED}\tERROR: $1 ${WHITE}"
	fi
}

function print_action() {
	if [[ "$1" ]]; then
		echo -e "${YELLOW}$1 ${WHITE}"
	fi
}

function print_success() {
	if [[ "$1" ]]; then
		echo -e "${GREEN}\t- $1 ${WHITE}"
	fi
}

function print_message() {
	if [[ "$1" ]]; then
		echo -e "${WHITE}\t$1"
	fi
}