#!/bin/bash
###############################################################################
# Name:		build_util.sh
# Purpose:	Granular build tasks
###############################################################################

function configure_distro_make_file() {
	print_action "=== Configuring distribution make file for current branch ==="
	
	`cat distro.make > temp.make`
	echo projects[$PROJECT][download][branch] = "$BRANCH_NAME"  >> temp.make
}

function cleanup_distro_make_file() {
	print_action "=== Removing intermediary make file ==="
	
	rm temp.make
}

function execute_full_build() {
	print_action "=== Executing Drush Make for Full Build ==="
	
	drush make --prepare-install temp.make $CODE_PATH --yes
}

function execute_package_build() {
	print_action "=== Executing Drush Make for Full Build and Archive ==="
	
	drush make --tar temp.make $RELEASES_PATH/$RELEASE_NAME --yes
}

function fix_permissions() {
	print_action "=== Setting user/group permissions ==="
	
	sudo chown -R $USER:$GROUP $CODE_PATH
	sudo chmod -R a=,u=rwX,g=rX $CODE_PATH
	sudo find $CODE_PATH/sites -type d -name files -exec chmod -R a=,ug=rwX '{}' \;
	sudo find $CODE_PATH/sites -type d -name files_private -exec chmod -R a=,ug=rwX '{}' \;
}

function execute_site_install() {
	print_action "=== Installing site to: mysqli://$PROJECT:$PROJECT@localhost/$BUILD_NAME ==="
	
	drush si $PROJECT --root=$CODE_PATH --db-url=mysqli://$PROJECT:$PROJECT@localhost/$BUILD_NAME --yes
}

function reset_sandbox() {
	print_action "=== Setting symlink sandbox to point to latest build ==="
	
	# Reset the sandbox link
	if [ -h $SANDBOX_PATH ]; then
		rm $SANDBOX_PATH
	fi
	
	# Create the sandbox link
	ln -s $CODE_PATH $SANDBOX_PATH
}

function delete_sandbox() {
	print_action "=== Removing sandbox symlink ==="
	if [ -h $SANDBOX_PATH ]; then
		rm $SANDBOX_PATH
		print_success "Symlink removed"
	else
		print_success "Symlink doesn't exist"
	fi
	
	return 0
}

function delete_build_by_name() {
	if [[ -z "$1" ]]; then
		print_error "[delete_build()] USAGE: This function takes the build name as a parameter"
		exit 1
	fi
	
	BUILD_DIR=${BUILDS_PATH}/${1}
	
	print_action "=== Deleting build ($1) ==="
	
	sudo rm -r $BUILD_DIR
	print_success "Removed files for ($1)"
	
	mysql -u$DBUSER --password=$DBPASS <<EOF
	DROP DATABASE $1;
EOF
    print_success "Dropped database ($1)"
	
	return 0
}