#!/bin/bash
###############################################################################
# Name:		ui.sh
# Purpose:	User Interface (menu options)
###############################################################################

function show_build_menu() {
	print_message "=== OPTIONS ==="
	print_message "[1] REBUILD - drush make the project in place (download modules, update database)"
	print_message "[2] RE-INSTALL - clear the database and install the profile (drush site-install)"
	print_message "\nSelection: \c"
	read SELECTION
	
	if [[ $SELECTION = "1" ]]; then
		rebuild
	else
		reinstall
	fi
}

function show_project_menu() {
	print_message "=== OPTIONS ==="
	print_message "[1] BUILD - build the current branch ($BUILD_NAME)"
	print_message "[2] DELETE CURRENT - delete the most recent build ($BUILD_NAME)"
	print_message "[3] DELETE SELECTED - choose a build to delete"
	print_message "[4] RELEASE - build a release ($RELEASE_NAME)"
	print_message "\nSelection: \c"
	read SELECTION
	
	if [[ $SELECTION = "1" ]]; then
		build_branch
	elif [[ $SELECTION = "2" ]]; then
		delete_current_build
	elif [[ $SELECTION = "3" ]]; then
		delete_arbitrary_build
	elif [[ $SELECTION = "4" ]]; then
		build_release
	fi
}

function show_finished_building_message() {
	print_success "*****************************************************************************"
	print_success "* If there are no errors, then branch: $BRANCH_NAME, was successfully"
	print_success "* built at: $CODE_PATH " 
	print_success "* Create a vhost entry in MAMP for domain: http://$PROJECT-$BRANCH_DIR.dev"
	print_success "*****************************************************************************"
}

function show_finished_release_message() {
	print_success "*****************************************************************************"
	print_success "* Release packaged in ($RELEASES_PATH/${RELEASE_NAME}.tar.gz)"
	print_success "*****************************************************************************"
}