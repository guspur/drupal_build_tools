#!/bin/bash
###############################################################################
# Name:		build.sh
# Purpose:	Configure and execute builds
###############################################################################

function set_build_paths() {
	DIR="$(pwd)"
	
	BUILDS_PATH="$DIR/builds"
	SANDBOX_PATH="$DIR/sandbox"
	RELEASES_PATH="$DIR/releases"
	
	# If the builds folder does not exist, make it
	if [ ! -d "$BUILDS_PATH" ]; then
		mkdir builds
		print_success "Created builds folder"
	fi
	
	if [ ! -d "$RELEASES_PATH" ]; then
		mkdir releases
		print_success "Created releases rolder"
	fi
}

function set_build_name() {
	BUILD_NAME=${PROJECT}_$BRANCH_DIR
	CODE_PATH=$BUILDS_PATH/$BUILD_NAME
}

function rebuild() {
	print_action "=== Rebuilding ==="
	
	# Remove contrib files because they will be re-downloaded with drush make
	if [ -d "modules/contrib" ]; then 
		print_action "=== Removing contrib modules ==="
		rm -r modules/contrib 
	fi
	if [ -d "themes/contrib" ]; then 
		print_action "=== Removing contrib themes ==="
		rm -r themes/contrib 
	fi
	if [ -d "libraries" ]; then 
		print_action "=== Removing libraries ==="
		rm -r libraries 
	fi
	
    # Run make only.
    print_action "=== Building $project profile ==="
    drush make --working-copy --no-core --contrib-destination=. ${PROJECT}.make --yes

	# Update database and features
	print_action "=== Updating database ==="
	drush updb -y
}

function reinstall() {
	print_action "=== Re-Installing ==="
	drush si $PROJECT -y
}

function build_branch() {
	print_action "=== Building Site ==="
	
	if remote_branch_exists "$BRANCH_NAME"; then
		######################################## 
		# Pre-build Tasks
		########################################
	    configure_distro_make_file
	
		######################################## 
		# Build Tasks
		########################################
	    execute_full_build
	
		######################################## 
		# Post-build Tasks
		########################################
		cleanup_distro_make_file
	
		create_database
	
	    fix_permissions

		execute_site_install
	
		reset_sandbox
	
		show_finished_building_message
	fi
}

function build_release() {
	print_action "=== Building Release ==="
	
	if remote_branch_exists "$BRANCH_NAME"; then
		######################################## 
		# Pre-build Tasks
		########################################
	    configure_distro_make_file
	
		######################################## 
		# Build Tasks
		########################################
		execute_package_build
		
		######################################## 
		# Post-build Tasks
		########################################
		cleanup_distro_make_file
		
		show_finished_release_message
	fi
}

function delete_current_build() {
	print_action "=== Deleting Current Build ==="
	
	delete_build_by_name $BUILD_NAME
	
	delete_sandbox
}

function delete_arbitrary_build() {
	print_action "=== Analyzing Builds ==="
	print_message "=== SELECT A BUILD ==="
	
	OPTION=1
	for BUILD in $(ls $BUILDS_PATH)
	do
		print_message "[$OPTION] $BUILD"
		BUILDS[$OPTION]=$BUILD
		OPTION=$((OPTION + 1))
	done
	
	print_message "\nSelection: \c"
	read SELECTION
	
	BUILD_NAME=${BUILDS[$SELECTION]}
	
	print_message "Are you sure you want to delete: $BUILD_NAME? [Y/n] \c"
	read SELECTION
	
	if [[ $SELECTION = 'Y' || $SELECTION = 'y' || -z "$SELECTION" ]]; then
		delete_build_by_name $BUILD_NAME
		exit 0
	else
		print_success "Canceled delete"
		exit 1
	fi
}